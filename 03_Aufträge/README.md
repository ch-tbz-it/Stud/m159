#### [01 AD Umgebung planen und VMs aufsetzen](./Aufträge/01_AD_Umgebung_planen_und_VMs_aufsetzen_formatted.md)
- 1 Server

#### [02 Router (pfsense) installieren & konfigurieren](./Aufträge/02_Router(pfsense)_installieren&konfigurieren.md)
- 2-3 Server

#### [03 Neue Gesamtstruktur](./Aufträge/03_Neue_Gesamtstruktur_custom.md)
- 1 Server

#### [04 Client in Domäne einbinden](./Aufträge/04_Client_in_Domäne_einbinden_custom.md)
- 1 Server

#### [05 Active Directory erste Schritte](./Aufträge/05_Active_Directory_erste_Schritte_custom.md)
- 1 Server

#### [06 DIT design und im AD anlegen](./Aufträge/06_DIT_design_und_im_AD_anlegen_custom.md)
- 1 Server

#### [07a DC2 zur Gesamtstruktur hinzufügen](./Aufträge/07a_DC2_zur_Gesamtstruktur_hinzufügen.md)
- 2-3 Server

#### [07b DC3 zur Gesamtstruktur hinzufügen](./Aufträge/07b_DC3_zur_Gesamtstruktur_hinzufügen.md)
- 2-3 Server

#### [08 DNS in ActiveDirectory](./Aufträge/08_DNS_in_ActiveDirectory.md)
- 2-3 Server

#### [09 GPOs](./Aufträge/09_GPOs_custom.md)
- 1 Server

#### [10 Standorte einrichten](./Aufträge/10_Standorte_einrichten.md)
- 2-3 Server

#### [11 GPO pro Standort](./Aufträge/11_GPO_pro_Standort.md)
- 2-3 Server

#### [13 Drucker mit GPO verteilen](./Aufträge/13_Drucker_mit_GPO_verteilen_custom.md)
- 1 Server

#### [14 Deploy MSI mit GPO](./Aufträge/14_Deploy_MSI_mit_GPO_custom.md)
- 2-3 Server

#### [15 ADDS-DNS und Replikation Fehleranalyse](./Aufträge/15_ADDS-DNS_und_Replikation_Fehleranalyse.md)
- 1 Server

#### [16 LDAP-Powershelltool](./Aufträge/16_LDAP-Powershelltool_custom.md)
- 1 Server

#### [17 DFS einrichten und Namespace anlegen](./Aufträge/17_DFS_einrichten_und_Namespace_anlegen_custom.md)
- 1 Server

#### [18 Testdomain einrichten](./Aufträge/18_Testdomain-einrichten.md)
- 4-5 Server

#### [19 Windows Admin Center einrichten](./Aufträge/19_Windows-Admin-Center-einrichten.md)
- 4-5 Server

#### [20 Azure AD mit AD-Connect einrichten](./Aufträge/20_Azure_AD_mit_AD-Connect_einrichten_custom.md)
- 1 Server

#### [21 Windows Client zu Azure AD hinzufügen](./Aufträge/21_Windows_Client_zu_Azure_AD_hinzufügen_custom.md)
- 1 Server

#### [22 Deploy MSI mit Intune](./Aufträge/22_Deploy_MSI_mit_Intune_custom.md)
- 1 Server

#### [23 Sicherheitsgruppe mit Office Lizenz verknüpfen](./Aufträge/23_Sicherheitsgruppe_mit_Office_Lizenz_verknüpfen_custom.md)
- 1 Server

#### [24 Bitlocker Festplattenverschlüsselung](./Aufträge/24_Bitlocker_Festplattenverschlüsselung_custom.md)
- 1 Server

#### [99 Idee für eigene Aufgaben](./Aufträge/99_Idee%20für%20eigene%20Aufgaben.md)

- 1 Server