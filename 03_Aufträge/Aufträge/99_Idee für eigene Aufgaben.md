# 99 Ideensammlung für eigene Aufgaben

> [!NOTE]
>
> Ich will, dass die Aufgaben von Modul 159 immer weiter wachsen. Dazu brauche ich eure Unterstützung. Habt ihr selber tolle Ideen für neue Aufgaben? Dann her damit! Ich freue mich über alle Ideen, die in die Aufgabensammlung von M159 aufgenommen werden.
>
> Falls Ihr Aufgabenstellung und Printscreen mit den verschiedene Schritten zu Verfügung stellt, gibt es Zusatzpunkte



## Azure

- Lokale Netzlaufwerke mit Azure Storage ersetzen
- MFA einrichten
- Drucker über Azure bereitstellen
- Diverse Azure CLI Befehle anwenden
  - Erstellen und Konfigurieren eines Benutzers ohne Passwort (Passwortlos)
  - Auditieren von Anmeldeaktivitäten eines Benutzers
- Azure Connect Cloud Sync
- Migration Multiple Domain Forest to Azure



## Samba

- Einen nicht MS-Domänencontroller in ein Active Directory integrieren



### OnPrem

- SSO einrichten