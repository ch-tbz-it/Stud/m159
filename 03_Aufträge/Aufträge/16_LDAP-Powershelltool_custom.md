# Übung 16

# 16.1 LDAP-Abfragen / LDAP-Queries (CMD-Prompt) (1 Punkt)

![Picture01](../images/16-Picture1.png)

Damit Sie diese LDAP-Befehle in der CMD ausführen können, müssen Sie die RSAT Tools für Ihren Server installieren. Sie finden eine Anleitung dazu im Ordner Tools.

## Auftrag 16.1.1: 

Erstellen Sie folgende vier AD-Objekte mit Dsadd:

- User

- Computer

- Kontakt

- Gruppe

## Auftrag 16.1.2:

Schreiben Sie eine LDAP-Abfrage mit «Dsquery», welche sämtliche User einer von Ihnen erstellten OU ausgibt

Schreiben Sie eine LDAP-Abfrage mit «Dsquery», welche sämtliche Computer einer erstellten OU ausgibt



## Auftrag 16.1.3:

Fügen Sie mit Dsmod einen User in eine Gruppe.



Auftrag 16.1.4:

Geben Sie einige Eigenschaften Ihrer Wahl des Kontaktes, welcher in Auftrag 1 erstellt wurde, mit DSget aus

## 

Auftrag 16.1.5:

Führen Sie die Beispiele (Examples) aus, welche unter dem QR-Code von «Dsmove» zu finden sind. Adaptieren Sie die Beispiele auf Ihre eigenen Objekte. 

Rename a user object from XY to YZ

Move the user object for XY from XY to the YZ organization

To combine the rename and move operations



Auftrag 16.1.6:

Löschen Sie mit «Dsrm» ein Testobjekt Ihrer Wahl



# Kontrolle

Dokumentieren Sie sämtliche Schritte also «Input und Rückmeldung» der LDAP-Abfrage mit Printscreens in einem separaten Dokument. Speichern Sie dieses Dokument mit der Namenskonvention «vorname.nachname» in Ihrem privaten Teams Kanal ab.

Erstellen Sie zusätzlich ein Screencast Video mit zwei Beispiel, um zu zeigen, dass Sie in der Lage sind, diese Befehle selbstständig auf Ihrer Umgebung auszuführen.






# 16.2 PowerShell-Tool (X Punkte)

Importieren Sie das Modul für Active Directory unter PowerShell.

![Picture01](../images/16-Picture2.png)



## Auftrag 16.2.1

Erstellen Sie ein PowerShell-Tool, welches AD-Aufgaben ausführen kann. Das PowerShell Tool soll ein Menu haben, dass entweder mit der Maus(grafisch) oder mit der Tastatur (Konsole) bedient werden kann.

Den Umfang Ihres Powershell-Tools können Sie frei wählen. Damit Sie wissen wie viele Punkte Ihnen der Lehrer dafür gibt besprechen Sie Ihr Vorhaben im Vorfeld mit der Lehrperson. 

> [!WARNING]
>
> Jede/r Lernende muss eine eigene Aufgabe für sein Tool definieren. Das Kopieren von Powershell-Skripten ist einfach, deshalb ist das unvermeidlich.



# Kontrolle

- Machen Sie ein Screencast wie man das Tool im Einsatz sieht und wie Sie die erforderlichen Funktionen ausführen.

- Speichern Sie das PowerShell Tool in Ihrem privaten Teams Kanal ab.

- Sie erhalten Punkte – und zwar individuell, je nach Umfang Ihrer PowerShell-Tools.

