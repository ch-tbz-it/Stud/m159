﻿#Hostname setzen
$Hostname = Read-Host "Geben sie den gewünschten Hostname an:"
Rename-Computer -NewName $Hostname

#IPv6 Disable
Write-Host "Disabling IPv6"
Disable-NetAdapterBinding -Name "Ethernet" -ComponentID ms_tcpip6

#Firewall Rules allowing Ping
Write-Host "Allowing Echo Request"
#create firewall manager object
$FWM=new-object -com hnetcfg.fwmgr

# Get current profile
$pro=$fwm.LocalPolicy.CurrentProfile

# Check Profile
if ($pro.IcmpSettings.AllowInboundEchoRequest) {
    "Echo Request already allowed"
} else {
    $pro.icmpsettings.AllowInboundEchoRequest=$true
}

# Display ICMP Settings
"Windows Firewall - current ICMP Settings:"
"-----------------------------------------"
$pro.icmpsettings

#Expolorer einstellungen
Write-Host "Erweiterungen bei unbekannten Dateitypen ausblenden"
$a = 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
Set-ItemProperty $a HideFileExt 0
Stop-Process -processname explorer

Write-Host -Foreground Yellow "Freigabeassistent verwenden wird deaktiviert."
Set-ItemProperty -Type DWord -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name "SharingWizardOn" -value "0"

Write-Host -Foreground Yellow "Geschuetzte Systemdateien ausblenden wird deaktiviert."
Set-ItemProperty -Type DWord -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name "ShowSuperHidden" -value "0"

#CMD Desktop verknüpfung
Write-Host -ForegroundColor Yellow "CMD Desktop verknüpfung"
$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("$Home\Desktop\cmd.lnk")
$Shortcut.TargetPath = "%windir%\system32\cmd.exe"
$Shortcut.Save()

#Enable Remote Desktop
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server' -name "fDenyTSConnections" -value 0
Enable-NetFirewallRule -DisplayGroup "Remote Desktop"

# UAC deaktivieren
Try {
	Set-ItemProperty -Path HKLM:\Software\Microsoft\Windows\CurrentVersion\policies\system -Name EnableLUA -Value 0
	Write-Host "UAC ist deaktiviert. Bitte Neustart durchführen." -ForegroundColor Green
} 
Catch {
	Write-Host "Fehler, die UAC konnte nicht deaktiviert werden!: $error[0]" -ForegroundColor Red
}

#Neustart
Write-Host -ForegroundColor Red "Restarting in 10sec"
Start-Sleep -s 10
Restart-Computer