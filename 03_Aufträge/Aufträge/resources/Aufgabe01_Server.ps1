﻿#Hostname setzen
$Hostname = Read-Host "Geben sie den gewünschten Hostname an:"
Rename-Computer -NewName $Hostname

#IPv6 Disable
Write-Host "Disabling IPv6"
Disable-NetAdapterBinding -Name "Ethernet" -ComponentID ms_tcpip6

#Firewall Rules allowing Ping
Write-Host "Allowing Echo Request"
#create firewall manager object
$FWM=new-object -com hnetcfg.fwmgr

# Get current profile
$pro=$fwm.LocalPolicy.CurrentProfile

# Check Profile
if ($pro.IcmpSettings.AllowInboundEchoRequest) {
    "Echo Request already allowed"
} else {
    $pro.icmpsettings.AllowInboundEchoRequest=$true
}

# Display ICMP Settings
"Windows Firewall - current ICMP Settings:"
"-----------------------------------------"
$pro.icmpsettings

#Sicherheitskonfigurationen von IE 
Write-Host -ForegroundColor Yellow "Verstärkte Sicherheitskonfiguration für IE deaktivieren"
#Disable Internet Explorer Enhanced Security for Admins
REG ADD „HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}“ /v „IsInstalled“ /t REG_DWORD /d 0 /f

#Disable Internet Explorer Enhanced Security for Users
REG ADD „HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A8-37EF-4b3f-8CFC-4F3A74704073}“ /v „IsInstalled“ /t REG_DWORD /d 0 /f

#Expolorer einstellungen
Write-Host "Erweiterungen bei unbekannten Dateitypen ausblenden"
$a = 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced'
Set-ItemProperty $a HideFileExt 0
Stop-Process -processname explorer

Write-Host -Foreground Yellow "Freigabeassistent verwenden wird deaktiviert."
Set-ItemProperty -Type DWord -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name "SharingWizardOn" -value "0"

Write-Host -Foreground Yellow "Geschuetzte Systemdateien ausblenden wird deaktiviert."
Set-ItemProperty -Type DWord -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name "ShowSuperHidden" -value "0"

Write-Host -ForegroundColor Yellow "CMD Desktop verknüpfung"
$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("$Home\Desktop\cmd.lnk")
$Shortcut.TargetPath = "%windir%\system32\cmd.exe"
$Shortcut.Save()

#Enable Remote Desktop
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server' -name "fDenyTSConnections" -value 0
Enable-NetFirewallRule -DisplayGroup "Remote Desktop"