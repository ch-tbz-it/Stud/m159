﻿#Hostname setzen
$Hostname = Read-Host "Geben sie den gewünschten Hostname an:"
Rename-Computer -NewName $Hostname

#IPv6 Disable
Write-Host "Disabling IPv6"
Disable-NetAdapterBinding -Name "Ethernet" -ComponentID ms_tcpip6

#Firewall Rules allowing Ping
Write-Host "Allowing Echo Request"
#create firewall manager object
$FWM=new-object -com hnetcfg.fwmgr

# Get current profile
$pro=$fwm.LocalPolicy.CurrentProfile

# Check Profile
if ($pro.IcmpSettings.AllowInboundEchoRequest) {
    "Echo Request already allowed"
} else {
    $pro.icmpsettings.AllowInboundEchoRequest=$true
}

# Display ICMP Settings
"Windows Firewall - current ICMP Settings:"
"-----------------------------------------"
$pro.icmpsettings


