$Path = Read-Host -Prompt "Enter the folder path" #fragt den Pfad vom CSV ab

$Users = Import-Csv -Delimiter ";" -Path $Path #Importiert CSV


$abfrage = Read-Host "Wollen Sie die Berechtigungen im CSV übernehmen? [Y]"

function smbcreate {
  if(!(Get-SMBShare -Name $Name -ea 0)){
    Write-Host -ForegroundColor Red "Creating Share $Name & NTFS permissions $Permission for $Abteilung on $Path as $Remote"
    New-SMBShare -Name "$Name" -Path $Path
    Grant-SmbShareAccess -Name $Name -AccountName Jeder -AccessRight Full
    $acl = Get-Acl $Remote
    $AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("$Abteilung","$Permission","Allow")
    $acl.SetAccessRule($AccessRule)
    $acl | Set-Acl $Remote
    Write-Host -ForegroundColor Green "Sucess $Name created"
  }
    else{
        Write-Host -ForegroundColor Red "Share does exist, creating NTFS permission $Permission for $Abteilung on $Remote"
        $acl = Get-Acl $Remote
        $AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule("$Abteilung","$Permission","Allow")
        $acl.SetAccessRule($AccessRule)
        $acl | Set-Acl $Remote
        Write-Host -ForegroundColor Green "Sucess NTFS on $Name"
    }

}

if($abfrage -eq "Y"){
foreach($User in $Users){

$Path = $User.FreigabePfad
$Name = $User.FreigabeName
$Permission = $User.Berechtigung
$Abteilung = "SKITTLE\" + $User.Abteilung
$Remote = "\\ws1\" + $Namesmbcreate
}
}
else{exit}