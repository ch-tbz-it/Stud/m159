﻿#Beschrieb: ADUser erstellen oder Löschen mit CSV
#Autor: Simone Piccolo
#Zuletzt bearbeitet am: 04.01.22

#=======================================================================

Import-Module ActiveDirectory

$Path = Read-Host -Prompt "Enter the folder path" #fragt den Pfad vom CSV ab
$PathLog =  Read-Host -Prompt "Enter the Lofile path" #Frag ab wo Logfile erstellt werden soll

$ErstelltLog = $PathLog + "/" + "BenutzerErstellt.txt" 
$DeleteLOG = $PathLog+ "/" + "BenutzerGelöscht.txt" 



$Users = Import-Csv -Delimiter ";" -Path $Path #Importiert CSV


$abfrage = Read-Host "Wollen Sie einen Benutzer erstellen oder löschen?" #Abfrage ob Löschen oder erstellen

if($abfrage -eq "erstellen"){ #Wenn die Abfrage, erstellen entspricht, wird ein Benutzer erstellt. 
foreach($User in $Users){ #Benutzer werden vom CSV in Variabeln gesetzt 
        
$Firstname = $User.Firstname
$Lastname = $User.Lastname 
$Password = $User.Password 
$Abteilung = $User.Abteilung
$Displayname = $User.Firstname + " " + $User.Lastname  

$OU = "Ou=$Abteilung, OU=Benutzer, Ou=skittle, DC=skittle, DC=ch" #Pfad vom AD wo User erstellt werden soll  

New-Aduser -Name "$Displayname" -GivenName "$Firstname" -Surname "$Lastname" -DisplayName "$Displayname" -AccountPassword (ConvertTo-SecureString $Password -AsPlainText -Force) -Path $OU -Enabled $true 
New-ADGroup "$Abteilung" -Path "OU=Gruppen, Ou=skittle, DC=skittle, DC=ch" -GroupCategory Security -GroupScope Global -PassThru –Verbose
Add-ADGroupMember -Identity $Abteilung -Members $Displayname

New-Item $ErstelltLog    #Logfile wird erstellt 
Set-Content $ErstelltLog "Der Benutzer $Displayname wurde erstellt" 
}
}
elseif($abfrage -eq "löschen"){  
foreach($User in $Users){ #Benutzer werden vom CSV in Variabeln gesetzt 
        
$Firstname = $User.Firstname
$Lastname = $User.Lastname 
$Password = $User.Password 
$Abteilung = $User.Abteilung
$Displayname = $User.Firstname + " " + $User.Lastname   


$OU = "Ou=$Abteilung, OU=Benutzer, Ou=Soltec, DC=soltec, DC=lan" #Pfad vom AD wo User erstellt werden soll  

Remove-ADUser -Identity $Displayname -Confirm:$False 

New-Item $DeleteLOG   #Logfile wird erstellt 
Set-Content $DeleteLOG "Der Benutzer $Diyplayname wurde gelöscht"  #Logfile wird erstellt  
}                                  #Wenn die Abfrage, löschen entspricht, wird der Benutzer mit dem Displaynamen, der im CSV mit Vornamen und Nachnamen angeben wird, gelöscht.
}
else{  #Wenn es keinem vom beiden entspricht 
exit
}
      

