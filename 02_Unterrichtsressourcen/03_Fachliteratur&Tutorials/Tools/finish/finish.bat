FOR /F "usebackq" %%i IN (`hostname`) DO SET MYVAR=%%i

set /p GRUPPENR=Gruppe Eingeben:

md C:\Temp\Gruppe%GRUPPENR%

rem msinfo32 /report C:\Temp\Gruppe%GRUPPENR%\infos.txt

echo\ >> C:\Temp\Gruppe%GRUPPENR%\systeminfos%computername%.txt
echo "******************************** HOSTNAME ********************************" >> C:\Temp\Gruppe%GRUPPENR%\systeminfos%computername%.txt

echo %computername% >> C:\Temp\Gruppe%GRUPPENR%\systeminfos%computername%.txt

echo\ >> C:\Temp\Gruppe%GRUPPENR%\systeminfos%computername%.txt
echo "******************************** DOMAIN CONTROLLERS********************************" >> C:\Temp\Gruppe%GRUPPENR%\systeminfos%computername%.txt

dsquery server >> C:\Temp\Gruppe%GRUPPENR%\systeminfos%computername%.txt

dsquery server 

echo\ >> C:\Temp\Gruppe%GRUPPENR%\systeminfos%computername%.txt
echo "******************************** IP CONFIG ********************************" >> C:\Temp\Gruppe%GRUPPENR%\systeminfos%computername%.txt

ipconfig /all >> C:\Temp\Gruppe%GRUPPENR%\systeminfos%computername%.txt

echo\ >> C:\Temp\Gruppe%GRUPPENR%\systeminfos%computername%.txt
echo "******************************** SID ********************************" >> C:\Temp\Gruppe%GRUPPENR%\systeminfos%computername%.txt

dsquery * -Filter "(name=%MYVAR%)" -attr objectSID >> C:\Temp\Gruppe%GRUPPENR%\systeminfos%computername%.txt

echo\ >> C:\Temp\Gruppe%GRUPPENR%\systeminfos%computername%.txt
echo "******************************** SITES ********************************" >> C:\Temp\Gruppe%GRUPPENR%\systeminfos%computername%.txt

dsquery site -o rdn >> C:\Temp\Gruppe%GRUPPENR%\systeminfos%computername%.txt

echo "******************************** FIREWALL-SETTINGS ********************************" >> C:\Temp\Gruppe%GRUPPENR%\systeminfos%computername%.txt

rem START /W REGEDIT /E "C:\Temp\Gruppe%GRUPPENR%\firewall%computername%.txt" HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\FirewallRules
rem REGEDIT /E C:\t.txt "HKEY_CURRENT_USER\Software"

netsh advfirewall show allprofiles state >> C:\Temp\Gruppe%GRUPPENR%\systeminfos%computername%.txt

pause