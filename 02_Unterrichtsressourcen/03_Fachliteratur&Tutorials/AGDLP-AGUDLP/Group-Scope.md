# Group Scope

| Feature                          | Domain Local                                                 | Global                                                       | Universal                                                    |
| :------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- |
|                                  |                                                              |                                                              |                                                              |
| Bereich                          | Einzelne Domäne                                              | Gesamte AD-Struktur                                          | Gesamte AD-Struktur                                          |
| Kann Mitglieder enthalten von    | Beliebige Domäne                                             | Nur eigene Domäne                                            | Beliebige Domäne                                             |
| Kann Berechtigungen erhalten für | Domäne in welcher die Gruppe erstellt wurde                  | Ressourcen in beliebiger Domäne                              | Ressourcen in beliebiger Domäne                              |
| Kann Mitglied sein von           | Andere lokale Domänengruppen, universelle Gruppen            | Lokale Domänen- und universelle Gruppen                      | Nur universelle Gruppen                                      |
| Replikation                      | Innerhalb der Domäne                                         | Innerhalb der Domäne                                         | Über die gesamte Gesamtstruktur                              |
| Anwendungsfall                   | Zuweisen von Berechtigungen für domänenspezifische Ressourcen | Gruppieren von Benutzern in einer bestimmten Domäne, die ähnliche Zugriffsanforderungen haben | Gruppieren von Benutzern aus mehreren Domänen, die ähnlichen Zugriff über die Gesamtstruktur hinweg benötigen |

Quelle: https://www.reddit.com/r/sysadmin/comments/17nm4ws/domainlocal_vs_universal_help_me/?tl=de&rdt=33558

