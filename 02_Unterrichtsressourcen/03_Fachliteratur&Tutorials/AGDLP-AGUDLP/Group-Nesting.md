# Group Nesting

## Was ist Group Nesting?

Personen werden in Rollengruppen eingeteilt, und die Rollengruppen sind in den Berechtigungsgruppen verschachtelt.

### Rollengruppen

Diese Gruppen repräsentieren typischerweise Rollen innerhalb einer Organisation, wie z. B. "Vertrieb", "IT", "HR" usw. Sie enthalten Benutzer, die dieselbe Rolle oder Funktion haben.

### Berechtigungsgruppen

Diese Gruppen werden verwendet, um spezifische Berechtigungen zu verwalten, wie z. B. Zugriff auf bestimmte Dateien, Ordner, Anwendungen oder Netzwerke.



## Beispiel

- Eine Rollengruppe "Vertrieb" enthält alle Mitarbeiter, die im Vertrieb arbeiten.
- Diese Rollengruppe "Vertrieb" wird dann einer Berechtigungsgruppe "Vertrieb-SharePoint-Zugriff" hinzugefügt, die spezifische Berechtigungen für den Zugriff auf bestimmte SharePoint-Bereiche gewährt.