# Berechtigungen unter Windows



## Ordnerstruktur erstellen

1. Erstellen Sie einen Ordner "C:\Mittelerde\"
2. Erstellen Sie einen Unterordner "C:\Mittelerde\Völker"
3. Erstellen Sie einen Unterordner "C:\Mittelerde\\Völker\Zwerge"
4. Erstellen Sie einen Unterordner "C:\Mittelerde\\Völker\Elben"
5. Erstellen Sie einen Unterordner "C:\Mittelerde\\Völker\Menschen"



## Gruppe erstellen und Vererbung erkennen

1. Fügen Sie eine neue Gruppe mit dem Namen "Menschen" zum Ordner "C:\Mittelerde" hinzu
2. Welche Änderungen können Sie bei den Ordner "Völker", Elben", Menschen" und "Zwerge" feststellen?



## Vererbungskette erkennen

<img align="left" width="auto" height="auto" src="https://gitlab.com/ch-tbz-it/Stud/m158/-/raw/main/04_Unterrichtsressourcen/03_%C3%9Cbungen/Berechtigungen%20unter%20Windows/Struktur.png?ref_type=heads">

1. Von wo erben "Elben", "Menschen" und "Zwerge" in Windows mit dieser Konfiguration standardmässig die Berechtigungen?
2. Von wo erbt "Völker" in Windows mit dieser Konfiguration standardmässig die Berechtigungen?
3. Von wo erbt "Mittelerde" in Windows mit dieser Konfiguration standardmässig die Berechtigungen?
4. Wenn ich bei Mittelerde die Vererbung deaktiviere, was ändert sich bei der Vererbung der Berechtigungen bei "Völker"?
5. Wenn ich bei Mittelerde die Vererbung deaktiviere, was ändert sich bei der Vererbung der Berechtigungen bei "Elben", "Menschen" und "Zwerge"?



## Auswirkungen beim deaktivieren von Vergebungen verstehen

1. Sie möchten nun die Gruppe "Menschen" beim Ordner "Elben" und "Zwerge" entfernen. Was müssen Sie tun?
2. Wenn Sie eine Vererbung auf einem Ordner ausschalten, erhalten Sie eine Frage mit zwei Optionen. Erklären Sie die beiden Optionen.
3. Wo kann ich im GUI bei Windows sehen, von wo ein Ordner seine Berechtigungen erbt?
4. Warum reichte es nicht bei Mittelerde die Vererbung zu deaktivieren um die Gruppe "Menschen" bei "Elben" und "Zwerge" zu entfernen?