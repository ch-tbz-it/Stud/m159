# M159 - Einstiegsfragen

Diese Fragen dienen zum aktivieren des Vorwissens in das Thema Active Directory Domain Services. Wer noch kein Vorwissen hat kann folgende Ressourcen nutzen. 

[What is a Windows Domain Controller](https://www.youtube.com/watch?v=cTe5GsyhKUk)

[3.1_Verzeichnisdienst.pdf](https://tbzedu.sharepoint.com/:b:/s/campus/students/EV49o9w43SxPnOcZ3v2Ey-EBLgUQozyJoiiHvBe9Ur0H5g?e=m0k9q6)

## Fragen

1. Aus technischer Sicht ist ein Verzeichnisdienst nichts anderes als

2. Nennen Sie 4 Kernaufgaben eines «Directory Services»

3. Nennen Sie 5 Objekte (Ressourcen), welche in einem Directory vorkommen können 

4. Nennen Sie 5 Merkmale eines «Directory Services»

5. Zusammenhang zwischen Objekt, Attribut und Wert 
   Kreuzen Sie die korrekten Antworten an: 

   ·	Benutzerattribute sind Eigenschaften von Computern 
   ·	Benutzerattribute haben einen Namen und einen Wert 
   ·	Das Benutzerpasswortattribut bestehend aus Namen und Wert ist ein Benutzerattribut 
   ·	Der Vorname ist ein Benutzerattribut 
   ·	Druckerobjekte haben keine Attribute 

6. Was ist eine Objektklasse?

7. Nennen Sie zwei Nachteile eines «Directory Services»

8. Beschreiben Sie eine IT-Situation, welche einen «Directory Service» verlangt

9. Beschreiben Sie eine IT-Situation, welche keinen «Directory Service» verlangt

10. Beschreiben Sie, was ein lokaler Administrator ist

11. Beschreiben Sie, was ein Domänenadministrator ist

12. Was ist die Aufgaben des Domänenkontroller’s? 

13. Wofür steht die Abkürzung DC? 

14. Wofür steht die Abkürzung OU bzw. OE? 

15. Was bedeutet „On-Premises“? 

16. Was ist der Unterschied zwischen MS AD DS und Entra ID

17. Was ist und macht Intune, MDM & MAM?

18. Was ist ein Tenant?

19. Wie nennt man eine AD-Umgebung mit Entra ID Integration



## Lösungen

[Einstiegsfragen](https://gitlab.com/ch-tbz-it/TE/m159/-/tree/main/L%C3%B6sungen?ref_type=heads)



