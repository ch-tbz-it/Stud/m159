# NSLOOKUP & DNS-Übung

1. Mit welchem DNS-Server arbeitet NSLOOKUP per Standard?
2. Ändern Sie den Abfrageserver in NSLOOKUP mit "server 8.8.8.8" auf einen anderen Server ab
3. Setzen Sie den Abfragetyp auf den Record NS mit dem Befehl "set type=NS"
4. Geben Sie einen "." ein und interpretieren Sie das Resultat. Was sehen Sie. Erklären Sie es in eigenen Worten
5. Geben Sie als Abfrageserver nun 198.41.0.4 an und machen Sie dort eine NS Abfrage auf "ch.". Erlären Sie in eigenen Worten was Sie sehen.
6. Wenn Sie nun den Server wieder auf 8.8.8.8 stellen und dieselbe NS-Record Abfrage auf "ch." sehen Sie zwar dasselbe Ergebnis aber es erscheint zusätzliche die Meldung: "Nicht autorisierende Antwort" . Erklären Sie, warum beim Server 198.41.0.4 (a.root-servers.net) diese Meldung nicht erscheint und beim DNS-Server 8.8.8.8 jedoch schon. 



