# Kompetenznachweise – Modul 159

> #### LB1 - Theorietest
>
> Die erste Leistungsbeurteilung ist eine theoretische Prüfung. Die Inhalte für die LB1 stammen aus den theoretischen Aufträgen, den Übungen und dem Glossar.
>
> ( Erreichte Punkte / Maximale Punkte ) x 5 + 1 = Note LB1 (0.1 gerundet)
>
> ### Gewichtung 1/3
>
> Themen:
>
> - [AD DS: DNS & Logonserver](https://gitlab.com/ch-tbz-it/Stud/m159/-/blob/main/02_Unterrichtsressourcen/04_%C3%9Cbungen/ADDS%20DNS%20Logonserver%20+%20DNS%20Server%20Verst%C3%A4ndnischeck%20Situation.pdf?ref_type=heads)
> - [Präsentation GPO](https://gitlab.com/ch-tbz-it/Stud/m159/-/blob/main/02_Unterrichtsressourcen/02_Pr%C3%A4sentationen/Gruppenrichtlinien.pptx?ref_type=heads)
> - Berechtigungen und Gruppen
>   - [Group Nesting](../02_Unterrichtsressourcen/03_Fachliteratur&Tutorials/AGDLP-AGUDLP/Group-Nesting.md)
>   - [Group Scope](../02_Unterrichtsressourcen/03_Fachliteratur&Tutorials/AGDLP-AGUDLP/Group-Scope.md)
>   - [Aufgabe 5](../03_Aufträge/Aufträge/05_Active_Directory_erste_Schritte_custom.md)
>   - [Übung Berechtigungen Windows](https://gitlab.com/ch-tbz-it/Stud/m159/-/blob/main/02_Unterrichtsressourcen/04_%C3%9Cbungen/Berechtigungen%20unter%20Windows/Berechtigungen%20unter%20Windows.md?ref_type=heads)
> - [Profile](../02_Unterrichtsressourcen/02_Präsentationen/Windows-Profile.pptx)
> - [DIT](../02_Unterrichtsressourcen/02_Präsentationen/DIT.pptx)
> - [Glossar (AD-Begriffe)](https://gitlab.com/ch-tbz-it/Stud/m159/-/blob/main/02_Unterrichtsressourcen/Glossar.md?ref_type=heads)

<hr>



> #### [LB2 - Aufträge](https://gitlab.com/ch-tbz-it/Stud/m159/-/tree/main/03_Auftr%C3%A4ge?ref_type=heads)
>
> Die zweite Leistungsbeurteilung resultiert aus den praktischen Aufträgen
>
> - Bewertung anhand vom [Bewertungsraster](https://gitlab.com/ch-tbz-it/Stud/m159/-/blob/main/01_Instruktionen/Bewertung.xlsx?ref_type=heads)
> - Das Bewertungsraster enthält Pflichtaufgaben. Nachdem diese abgeschlossen wurden, kann frei gewählt werden. 
>
> ( Erreichte Punkte / Maximale Punkte ) x 5 + 1 = Note LB2 (0.1 gerundet)
>
> ### Gewichtung 2/3

<hr>



> ### Modulnote
>
> (LB1 x 0.3334) + (LB2 x 0.667) = Modulnote (0.5 gerundet)

