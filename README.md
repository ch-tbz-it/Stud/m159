# M159 - Directoryservices konfigurieren und in Betrieb nehmen

Herzlich willkommen im Modul 159! 



## Version der Modulidentifikation

13.08.2021, V3 ([Modulbaukasten ICT-BCH](https://www.modulbaukasten.ch/module/159/3/de-DE?title=Directoryservices-konfigurieren-und-in-Betrieb-nehmen)).



## Kurzbeschreibung des Moduls gemäss Modulidentifikation

*Directoryservices konfigurieren, Directorystrukturen implementieren, Benutzerdaten verwalten, Sicherheitseinstellungen nach Vorgabe vornehmen, Synchronisation sicherstellen, Directoryservices testen und an Betrieb übergeben.*



## Aufbau der Unterlagen zum Modul

- 01_Instruktionen
- 02_Unterrichtsressourcen
  - 01_Bilder
  - 02_Präsentationen
  - 03_Fachliteratur&Tutorials
  - 04_Übungen
  - 05_Umfrage
  
- 03_Aufgabe



### 01_Instruktionen

Hier erfahren Sie wie das Modul aufgebaut ist und durchgeführt wird.

### 02_Unterrichtsressourcen

Hier finden Sie alle Unterlagen, von der Fachliteratur über Präsentationen bis hin zu Übungsaufgaben, die Sie zur Bearbeitung und Lösung der Aufgaben benötigen. 

### 03_Aufträge

Hier finden Sie alle Aufträge für die Bewertung Ihrer LB2.



### 04_Umsetzungsvorschlag

Hier finden Sie den Modulplan mit den spezifischen Themen pro Tag. Hier finden Sie die Inhalte, die während des Moduls behandelt werden. Der Umsetzungsvorschlag dient vor allem als Orientierungshilfe für die Lehrenden, die das Modul unterrichten.
